//
//  MapsViewController.swift
//  Trevel App
//
//  Created by BOTTAK on 4/24/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


protocol MapViewControllerDelegate {
    func mapControllerSelectDidPoint (_ point:MKPointAnnotation)
}

class MapsViewController: UIViewController {
    
    //MARK: - Properties
    

    
    var geoDouble: [MKPointAnnotation]?
    var delegate: MapViewControllerDelegate?

    
    
    @IBAction func pointSelectedGesture(_ recognizer: UITapGestureRecognizer) {
        let point = recognizer.location(in: mapView)
        let tapPoint = mapView.convert(point, toCoordinateFrom: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = tapPoint
        
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
    }
    
    //MARK: - Outlet
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let geoDouble = geoDouble {
            for point in geoDouble {
                mapView.addAnnotation(point)
            }
        }
        
    }
    

    
    
    
    //MARK: - Actions
    @IBAction func saveClicked(_ sender: UIButton) {
        if let selectedPoint = mapView.annotations.first as? MKPointAnnotation {
             delegate?.mapControllerSelectDidPoint(selectedPoint)


        }
        
        navigationController?.popViewController(animated: true)
    }
}


