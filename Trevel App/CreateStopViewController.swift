//
//  CreateStopViewController.swift
//  Trevel App
//
//  Created by BOTTAK 4/10/19.
//  Copyright © 2019 BOTTAK. All rights reserved.
//

import UIKit
import MapKit

protocol ConclusionViewControllerDelegate {
    func conclusionViewController(_ stop: RealmStop)
}


protocol CreateStopViewControllerDelegate {
    func createStopViewController(_ stop: RealmStop)
}

class CreateStopViewController: UIViewController {
    
    
    //MARK: - Outlet
    
    var delegate: ConclusionViewControllerDelegate?
    var delegate1: CreateStopViewControllerDelegate?
    @IBOutlet weak var choiseTransp: UISegmentedControl!
    @IBOutlet weak var stepView: UIStepper!
    @IBOutlet weak var stepLabel: UITextField!
    
    @IBOutlet weak var DiscTextField: UITextView!
    @IBOutlet weak var spendMoneyLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mapsSave: UILabel!
    
    var stop = RealmStop()
    var choiseTransport = UISegmentedControl ()
    var stopDidCreateClosure: ((RealmStop) -> Void)?
    
    //MARK: - Action
    
    @IBAction func dismissButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoneyButton(_ sender: UIButton) {
        let controllerFromStoryBoard = UIViewController.getFromStoryboard(withId: "SpendManeyViewController") as? SpendManeyViewController
        if let spendMoneyVC = controllerFromStoryBoard as? SpendManeyViewController {
            spendMoneyVC.delegate = self
            navigationController?.pushViewController(controllerFromStoryBoard!, animated: true)
        }
        
        
    }
    
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        if let name = nameTextField.text{
            stop.name = name
            
        }
        if let stepname = stepLabel.text,  let stepNameInt = Int(stepname) {
            stop.rating = stepNameInt
            
        }
        if let disc = DiscTextField.text {
            stop.decs = disc
        }
        if let mcount = spendMoneyLabel.text {
            stop.spendMoney = mcount
        }
//        delegate1?.createStopViewController(stop)
        stopDidCreateClosure?(stop)
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func stepAction(_ sender: UIStepper) {
        stepLabel.text = String(Int(stepView.value))
    }
    
    
    
    @IBAction func choiseTransp(_ sender: UISegmentedControl) {
        if choiseTransp.selectedSegmentIndex == 0 {
            stop.transportType = .samolet
        } else {
            
            if choiseTransp.selectedSegmentIndex == 1 {
                stop.transportType = .poezd
            } else {
                if choiseTransp.selectedSegmentIndex == 2 {
                    stop.transportType = .autobus
                }
            }
        }
    }
    
    
    @IBAction func mapButtonClicked(_ sender: UIButton) {
        let minsk = MKPointAnnotation()
        minsk.coordinate = CLLocationCoordinate2D(latitude: 53.925, longitude: 27.508)
        
        let moscow = MKPointAnnotation()
        moscow.coordinate = CLLocationCoordinate2D(latitude: 55.668, longitude: 37.689)
        
        let mapsController = UIViewController.getFromStoryboard(withId: "MapsViewController") as! MapsViewController
        mapsController.geoDouble = [minsk, moscow]
        mapsController.delegate = self
        navigationController?.pushViewController(mapsController, animated: true)
    }
    
    func userSpendMoney(_ moneyCount: String, currenty: CurrentyType) {
        // print("\(moneyCount) ")
        // label.text = moneyCount
        spendMoneyLabel.text = moneyCount
       
    }
}


extension CreateStopViewController: MapViewControllerDelegate {
    func mapControllerSelectDidPoint(_ point: MKPointAnnotation) {
         mapsSave.text = "\(point.coordinate.latitude) - \(point.coordinate.longitude)"
        print("Найти в поиске эту строчку, она поможет тебе вывести в лейбл нужный текст с локацией")
    }
}


extension CreateStopViewController: SpendManeyViewControllerDelegate  {
    func spendManeyViewController(money: String, curency: CurrentyType) {
        self.spendMoneyLabel.text = "\(money) \(curency.rawValue)"
        
        stop.spendMoney = money
        stop.currency = curency.rawValue 
        
    }
    
    
    
}


