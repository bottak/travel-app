//
//  forGotPasswordViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

//MARK: - Action

class ForGotPasswordViewController: UIViewController {
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
}
