//
//  LoginViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/11/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    //MARK: - NextPageAction
    
    @IBAction func LoginWitchEmail(_ sender: UIButton) {
//        let  storyboard = UIStoryboard.init(name: "Main", bundle: nil) // Обращение к StoryBoard, создается переменная для работы с ним
        
//        let loginController = storyboard.instantiateViewController(withIdentifier: "LoginWithEmailViewController") // StoryBoard найди пожалуйста в себе такой экран, у которого id вот такой LoginWithEmailViewController
        
//        present(loginController, animated: true, completion: nil) // Вызывается системная функция present и указывается что показвыать
        let loginController = UIViewController.getFromStoryboard(withId: "LoginWithEmailViewController")
        navigationController?.pushViewController(loginController!, animated: true)
    }
    
    
    @IBAction func registerClicket(_ sender: UIButton) {
        let loginController = UIViewController.getFromStoryboard(withId: "LoginWithEmailViewController")
        navigationController?.pushViewController(loginController!, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        

        }
    }
    
