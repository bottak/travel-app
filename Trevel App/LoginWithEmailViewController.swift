//
//  LoginWithEmailViewController.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/16/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class LoginWithEmailViewController: UIViewController {
    
    //MARK: Outlet
    
    var emailTextField: String = "111"
    var passwordTextField: String = "111"
    
    @IBOutlet weak var eyeTwo: UIButton!
    @IBOutlet weak var eyeOne: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var incorrectPasswordLabel: UILabel!
    @IBOutlet weak var passwordUnderlinedView: UIView!
    
    //MARK: - Actions
    @IBAction func eyeTwoAction(_ sender: Any) {
        eyeOne.isHidden = true
        eyeTwo.isHidden = false
    }
    @IBAction func eyeOneAction(_ sender: Any) {
        eyeOne.isHidden = false
        eyeTwo.isHidden = true
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if password.text == passwordTextField, loginTextField.text == emailTextField {
            incorrectPasswordLabel.isHidden = true
            let tripsController = UIViewController.getFromStoryboard(withId: "TripsViewController")
            navigationController?.pushViewController(tripsController!, animated: true)
        
        } else {
            incorrectPasswordLabel.isHidden = false
            incorrectPasswordLabel.text = "Password"
            incorrectPasswordLabel.textColor = .red
            passwordUnderlinedView.backgroundColor = .red
        }
    }
    
    //MARK: - ActionsNextPage
    
    @IBAction func hideButton(_ sender: Any) {
       password.isSecureTextEntry.toggle()
    }
    
  
   
    @IBAction func backClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func forgotClicked(_ sender: UIButton) {
        let loginController = UIViewController.getFromStoryboard(withId: "ForGotPasswordViewController")
        loginController?.view.backgroundColor = .red
        navigationController?.pushViewController(loginController!, animated: true)
        
    }
    
//    @IBAction func newTravelClicked(_ sender: UIButton) {
//        let loginController = UIViewController.getFromStoryboard(withId: "TripsViewController")
//        navigationController?.pushViewController(loginController!, animated: true)
//    }
//    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
//    loginButtonTapped.layer.cornerRadius = 5

    }
    


}
