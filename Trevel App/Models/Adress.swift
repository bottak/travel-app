//
//  Adress.swift
//  Trevel App
//
//  Created by BOTTAK on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

class Adress {
    var street: String?
    var suit: String?
    var city: String?
    var zipcode: String?
    var geo: Geo?
    
}

class Geo {
    var latitude: String?
    var longitude: String?
}



