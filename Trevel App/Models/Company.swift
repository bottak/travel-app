//
//  Company.swift
//  Trevel App
//
//  Created by BOTTAK on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

class Company {
    var name: String = ""
    var catchPhrase: String = ""
    var bs: String = ""
}
