//
//  RealmGeo.swift
//  Trevel App
//
//  Created by BOTTAK on 6/1/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import RealmSwift

class RealmGeo: Object {
    @objc dynamic var latitude: String = ""
    @objc dynamic var langitude: String = ""
}
