//
//  RealmStop.swift
//  Trevel App
//
//  Created by BOTTAK on 6/1/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import RealmSwift


class RealmStop: Object {
    
    @objc dynamic var id: String = UUID() .uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var rating: Int = 0
    @objc dynamic var spendMoney: String = ""
    @objc dynamic var currency: String = ""
    @objc dynamic var decs: String = ""
    @objc dynamic var location: String = ""
    @objc dynamic var transportType: TransportType = .autobus
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    }
    
    

