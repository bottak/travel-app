//
//  RealmUser.swift
//  Trevel App
//
//  Created by BOTTAK on 5/29/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import RealmSwift

class RealmUser: Object {
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
