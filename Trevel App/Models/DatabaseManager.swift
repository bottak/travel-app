//
//  DataBaseManager.swift
//  Trevel App
//
//  Created by BOTTAK on 5/29/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseManager {
    
    static var instance = DatabaseManager()
    
    
    func save<T>(_ objects: [T]) {
        
        let realm = try! Realm()
        try! realm.write {
            if let objects = objects as? [Object] {
                realm.add(objects, update: true)
            }
        }
    }
    
    
    func save<T: Object>(_ objects: [T]) {
        
        let realm = try! Realm()
        try! realm.write {
        realm.add(objects, update: true)
        }
    }
    
    
    func save<T: Object, S>(_ objects: [T], types: S) {
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(objects, update: true)
        }
    }
    
    func getObjects<T: Object>(_ classType: T.Type) -> [T] {
        let realm = try! Realm()
        let result = realm.objects(T.self)
        return Array(result)
    }
    
    
    
    
    
    func saveToDatabase(users: [RealmUser]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(users, update: true)
//            print("\(Realm.Configuration.defaultConfiguration.fileURL)")
        }
    }
    
    func saveTravelToDatabase(travel: RealmTravel){
        let realm = try! Realm()
        //        print("\(Realm.Configuration.defaultConfiguration.fileURL)")
        try! realm.write {
            realm.add(travel, update: true)
        }
    }
    
    func getUsersFromDatabase() -> [RealmUser] {
        let realm = try! Realm()
        let usersResult = realm.objects(RealmUser.self) // хранится побобие массива только Reaml-мовского
        
        var users: [RealmUser] = []
        for realmUser in usersResult {
            users.append(realmUser)
        }
        //        usersResult.count
        //        usersResult[0]
        return users
    }
    
    func getTravelsFromDatabase() -> [RealmTravel] {
        let realm = try! Realm()
        let realmTravelsRuselt = realm.objects(RealmTravel.self)
        
//        var travels: [RealmTravel] = []
//        for realmTravel in realmTravelsRuselt {
//            let travel = RealmTravel()
//            travel.name = realmTravel.name
//            travel.desc = realmTravel.description
//            travels.append(travel)
//        }
        return Array(realmTravelsRuselt)
    }
    
    func getTravelFromDatabase(withId id: String) -> RealmTravel? {
        let realm = try! Realm()
        let travel = realm.object(ofType: RealmTravel.self, forPrimaryKey: id)
        return travel
    }
    
    func updateTravel(_ travel: RealmTravel, withName name: String) {
        let realm = try! Realm()
        try! realm.write {
            travel.name = name
        }
    }
 
    func addStop (_ stop: RealmStop, to travel: RealmTravel) {
        let realm = try! Realm()
        try! realm.write {
            travel.stops.append(stop)
        }
    }
    
    
    func deleteTravels (_ travel: RealmTravel) {
       
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.delete(travel)
        }
    }
    
    
    func deleteStop (_ stop: RealmStop) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(stop)
        }
    }
    
    
}

