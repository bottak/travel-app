//
//  Stop.swift
//  Trevel App
//
//  Created by BOTTAK on 4/19/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import UIKit


class Stop {
    var name: String = ""
    var rating: Int = 1
    var spendMoney: String = ""
    var transportType: TransportType?
    var currency: CurrentyType?
    var decs: String = ""
}

@objc enum TransportType: Int {
    case autobus, samolet, poezd
}

enum CurrentyType : String {
    case dollar = "$"
    case euro = "€"
    case ruble = "₽"
}



//func printAll() {
//    print(
//        "Название" - \(name)
//        "Рейтинг" - \(rating)
//        "Потратил" - \(spendMoney)
//        "Транспорт" - \(transportType)
//        "Описание" - \(decs)
//        )
//}
