//
//  Travel.swift
//  Trevel App
//
//  Created by BOTTAK on 5/10/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation

class Travel {
    var name: String = ""
    var desc: String = ""
    var stops: [Stop] = []
    
    func getAvarageRating() -> Int {
        if stops.count <= 0 {
            return 0
        }
        var summ: Int = 0
        for stop in stops {
            summ = summ + stop.rating
        }
        return summ/stops.count
    }
}


