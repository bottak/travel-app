//
//  User.swift
//  Trevel App
//
//  Created by BOTTAK on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation



class User: CustomStringConvertible {
    var id: Int!
    var name: String?
    var username: String?
    var phone: String?
    var email: String?
    var website: String?
    var company: Company?
    var geo: Geo?
    var adress: Adress?
    
    var description: String {
        return """
        id - \(id)
        name - \(name)
        username - \(username)
        email - \(email)
        phone - \(phone)
        website - \(website)
        
        Adress:
        street - \(adress?.street)
        suite - \(adress?.suit)
        city - \(adress?.city)
        zipcode - \(adress?.zipcode)
        
        Geo:
        latitude - \(adress?.geo?.latitude)
        longitude - \(adress?.geo?.longitude)
        
        Company:
        name - \(company?.name)
        catchPhrase -\(company?.catchPhrase)
        bs - \(company?.bs)
        """
    }
}
    
    
    
//    func printAll() {
//        print("""
//            \n
//            User:
//            id - \(id)
//            name - \(name)
//            username - \(username)
//            email - \(email)
//            phone - \(phone)
//            website - \(website)
//
//            Adress:
//            street - \(adress?.street)
//            suite - \(adress?.suit)
//            city - \(adress?.city)
//            zipcode - \(adress?.zipcode)
//
//            Geo:
//            latitude - \(adress?.geo?.latitude)
//            longitude - \(adress?.geo?.longitude)
//
//            Company:
//            name - \(company?.name)
//            catchPhrase -\(company?.catchPhrase)
//            bs - \(company?.bs)
//            \n
//            """)
//    }
//
//}
//
////func printAll() {
////    print(""""
////        \n
////        User:
////
////
////        """")
////}

