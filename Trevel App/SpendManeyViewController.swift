//
//  fiftрViewcontroller.swift
//  Trevel App
//
//  Created by Stanislau Reut on 4/10/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import Foundation
import UIKit

protocol SpendManeyViewControllerDelegate {
    func spendManeyViewController(money: String, curency: CurrentyType)
}


class SpendManeyViewController:UIViewController{
    
    //MARK: - Outlet
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var viewTextFild: UIView!
    
    //MARK: - Properties
    var delegate: SpendManeyViewControllerDelegate?
    var currenty: CurrentyType = .dollar
    
    //MARK: - Action
    @IBAction func closeButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func readyClicked(_ sender: UIButton) {
        if segmentedControl.selectedSegmentIndex == 0 {
            delegate?.spendManeyViewController(money: textField.text!, curency: .dollar)
        } else if segmentedControl.selectedSegmentIndex == 1 {
            delegate?.spendManeyViewController(money: textField.text!, curency: .euro)
        } else if segmentedControl.selectedSegmentIndex == 2 {
            delegate?.spendManeyViewController(money: textField.text!, curency: .ruble)
        }
        
        navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func sendSpendButtonClicked(_ sender: UIButton) {
//        let loginController = UIViewController.getFromStoryboard(withId: "CreateStopViewController")
//        navigationController?.pushViewController(loginController!, animated: true)
//    }
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTextFild.layer.cornerRadius = 10
        button.layer.cornerRadius = 10
    }
    
}
// Autolayout constraint
