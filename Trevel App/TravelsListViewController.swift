//
//  TravelsListViewController.swift
//  Trevel App
//
//  Created by BOTTAK on 4/26/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import RealmSwift

class TravelsListViewController: UIViewController {
    
    var travels : [RealmTravel] = []
    var text: String = ""
    
    
    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var stopListLabel: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    
    //MARK: - Lifecycle
    
    //MARK: - Action
    
    
    
    
    @IBAction func addTravelList(_ sender: UIButton) {
        let alert = UIAlertController(title: "Add your travel", message: "Enter country and description", preferredStyle: .alert)
        
        //add TextFields in alert
        alert.addTextField()
        alert.addTextField()
        //set properities for TextField
        alert.textFields![0].placeholder = "Введите страну"
        alert.textFields![1].placeholder = "Введите описание"
        //add action buttons Save and Cancel
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: {(action) in
            print("Отмена") // Show in log
        }))
        
        alert.addAction(UIAlertAction(title: "Доб.", style: .default, handler: {(action) in
            let country = alert.textFields![0].text
            let description = alert.textFields![1].text
            //set to outlets
            let realmTravel = RealmTravel()
            realmTravel.name = country ?? ""
            realmTravel.desc = description ?? ""
            
//            DatabaseManager.instance.saveTravelToDatabase(travel: realmTravel)
            DatabaseManager.instance.save([realmTravel])
           
            
            self.travels.append(realmTravel)
            self.tableView.reloadData()
        }))
        
        present(alert, animated: true)
    }
    
    // Экран показан, но без верстки
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
//        travels = DatabaseManager.instance.getTravelsFromDatabase()
        travels = DatabaseManager.instance.getObjects(RealmTravel.self)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    // Почти показали
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // Показали и уже есть верстка
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // Срабатывает за долю сенкунды до закрытия
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // Срабатыает сразу после того как экран исчез
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    // Функция срабатывает когда он освобождается из памяти (полностью удален)
    deinit {
        
    }
    // Каждый раз когда какой-то констрейнт решит измениться (обновиться) - пример переворот экрана
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // Функция срабатывает когда переворачивается экран
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    // Когда происходит утечка памяти (бесполезная) перелимит по использованию оперативной памяти
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



extension TravelsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if travels.count == 0 {
            stopListLabel.isHidden = false
        } else {
            stopListLabel.isHidden = true
        }
        return travels.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let stopVC = storyboard.instantiateViewController(withIdentifier: "StopsViewController") as! StopsViewController
        stopVC.realmTravel = travels[indexPath.row]
        stopVC.delegate = self
        stopVC.name = travels[indexPath.row].name
        navigationController?.pushViewController(stopVC, animated: true)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "call1", for: indexPath) as! TripCell  // Обращение к таблице и говорим дай пожалуйста ячейку, которую ты там в себе переиспользуешь, и дай мне именно ту ячейку с айди, которая мне сейчас нужна.
        let travel = travels[indexPath.row]
        
        cell.travelNameLabel.text = travel.name
        cell.travelSubtitleLabel.text = travel.desc
        cell.travelView.layer.shadowColor = UIColor.gray.cgColor
        cell.travelView.layer.shadowOpacity = 1
        cell.travelView.layer.shadowOffset = .zero
        cell.travelView.layer.shadowRadius = 5
        cell.travelView.layer.cornerRadius = 15
        let average = travel.getAvarageRating()
        if average == 1{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = false
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if average == 2{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if average == 3{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if average == 4{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = false
        }
        if average == 5{
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == UITableViewCell.EditingStyle.delete {
            DatabaseManager.instance.deleteTravels(travels[indexPath.row])
            travels.remove(at: indexPath.row)
            
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            tableView.endUpdates()
        }
    }
}


extension TravelsListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            travels = DatabaseManager.instance.getTravelsFromDatabase()
            tableView.reloadData()
            return
        }
        travels = travels.filter({ realmTravel -> Bool in
            realmTravel.name.lowercased().contains(searchText.lowercased())
            
        })
        tableView.reloadData()
    }
}




