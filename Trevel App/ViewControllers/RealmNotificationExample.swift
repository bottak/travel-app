//
//  RealmNotificationExample.swift
//  Trevel App
//
//  Created by BOTTAK on 6/5/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit
import RealmSwift
class RealmNotificationExample: UIViewController {
    
    //MARK: - Properties
    var travel: RealmTravel?
    var notificationToken: NotificationToken? = nil
    
    //MARK: - Outlets
    @IBOutlet weak var resultLabel: UILabel!
    
    
    
    //MARK: - Action
    @IBAction func createTravel(_ sender: UIButton) {
        travel = RealmTravel()
        travel?.id = "xxxxxx"
        travel?.name = "asdsadasda"
        DatabaseManager.instance.saveTravelToDatabase(travel: travel!)
        notificationToken = travel?.observe( { (changes) in
            switch changes {
            case .change(let properties):
                for property in properties {
                    if property.name == "name" {
                        self.resultLabel.text = property.newValue as? String
//                        self.notificationToken = nil
                    }
                }
            case .error(let error):
                print("An error occurred: \(error)")
            case .deleted:
                print("The object was deleted.")
            }
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        
    }
    

    

}
