//
//  StopsViewController.swift
//  Trevel App
//
//  Created by BOTTAK on 5/3/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class StopsViewController: UIViewController {

    
    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var stopNameLabel: UINavigationItem!
    
    var name: String = ""

    
    
    
    //MARK: - Action
    @IBAction func createStops(_ sender: Any) {
        if let createVC = UIViewController.getFromStoryboard(withId: "CreateStopViewController") as? CreateStopViewController{
            
            navigationController?.pushViewController(createVC, animated: true)
//            createVC.delegate1 = self
            createVC.stopDidCreateClosure = { stop in
//            self.realmTravel.stops.append(stop)
                DatabaseManager.instance.addStop(stop, to: self.realmTravel)
                DatabaseManager.instance.saveTravelToDatabase(travel: self.realmTravel)
            }
        }
    }
    
    
 

    
//    @IBAction func backRating(_ sender: Any) {
//        count = arrayStopRaiting.count
//        for index in arrayStopRaiting {
//            summArrayStopRating += index
//        }
//        delegate?.average = summArrayStopRating/(count+1)
//        delegate?.tableView.reloadData()
//        navigationController?.popViewController(animated: true)
//    }
    
    
    //MARK: - Properties
//    var travel: Travel!
    var delegate: TravelsListViewController?
    var realmTravel: RealmTravel!
    
    
    
    //MARK: - Lifecycle
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = name
//        stopNameLabel.text = name
        
//        let stop1 = Stop()
//        stop1.name = "Minsk"
//        stop1.decs = "Hello Minsk"
//
//        let stop2 = Stop()
//        stop2.name = "Rome"
//        stop2.decs = "Hello Rome"
//
        tableView.delegate = self
        tableView.dataSource = self
//
//        stops.append(stop1)
//        stops.append(stop2)
        
       

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

}




extension StopsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realmTravel.stops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopsCell", for: indexPath) as! StopsCell
        let stop = realmTravel.stops[indexPath.row]
        cell.stopTitleLabel.text = stop.name
        cell.stopSubtitleLabel.text = stop.decs
        cell.stopSpendMoney.text = stop.spendMoney
        cell.tableView.layer.shadowColor = UIColor.gray.cgColor
        cell.tableView.layer.shadowOpacity = 1
        cell.tableView.layer.shadowOffset = .zero
        cell.tableView.layer.shadowRadius = 5
        cell.tableView.layer.cornerRadius = 15
        
        cell.tableView.layer.shouldRasterize = true
        cell.tableView.layer.rasterizationScale = UIScreen.main.scale
     
        if stop.transportType == .samolet {
            cell.samoletImage.isHidden = false
            cell.autobusImage.isHidden = true
            cell.poezdImage.isHidden = true
        }
        if stop.transportType == .autobus {
            cell.autobusImage.isHidden = false
            cell.poezdImage.isHidden = true
            cell.samoletImage.isHidden = true
        }
        if stop.transportType == .poezd {
            cell.poezdImage.isHidden = false
            cell.samoletImage.isHidden = true
            cell.autobusImage.isHidden = true
        }
        
        if stop.rating == 1 {
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = false
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if stop.rating == 2 {
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = false
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if stop.rating == 3 {
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = false
            cell.star5.isHighlighted = false
        }
        if stop.rating == 4 {
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = false
        }
        if stop.rating == 5 {
            cell.star1.isHighlighted = true
            cell.star2.isHighlighted = true
            cell.star3.isHighlighted = true
            cell.star4.isHighlighted = true
            cell.star5.isHighlighted = true
        }
        
        
        
        return cell
    }
    
    
    
}

extension StopsViewController: CreateStopViewControllerDelegate {
    func createStopViewController(_ stop: RealmStop) {
        realmTravel.stops.append(stop)
        tableView.reloadData()
    }
    
//    func createStopViewController(_ stop: RealmStop) {
////        realmTravel.stops.append(stop)
//        tableView.reloadData()
    
    }
    
    




