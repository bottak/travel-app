//
//  UsersListViewController.swift
//  Trevel App
//
//  Created by BOTTAK on 5/17/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class UsersListViewController: UIViewController {
    var users: [RealmUser] = []
    var text: String = ""
    
    
    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    //MARK: - Action
    
    
    
    //MARK: - Property
    
    
    
    
    //    override func viewDidLoad() {
    //        super.viewDidLoad()
    //        ApiManager.instance.getUsers { (usersFromServer) in
    //            self.users = usersFromServer
    //
    //
    //
    //        }
    //
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        users = DatabaseManager.instance.getUsersFromDatabase()
//        ApiManager.instance.getUsers { (usersFromServer) in
//            for user in usersFromServer {
//                let reamlUser = RealmUser()
//                reamlUser.id = String(user.id)
//                reamlUser.name = user.name ?? ""
//                reamlUser.email = user.email ?? ""
//                self.users.append(reamlUser)
//            }
////            self.users = usersFromServer
//            DatabaseManager.instance.saveToDatabase(users: self.users)
//            self.tableView.reloadData()
//        }
    }
}


extension UsersListViewController: UITabBarDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let user = users[indexPath.row]
        cell.textUser.text = user.name
       
        
        return cell
    }
    
}
