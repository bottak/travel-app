//
//  СonclusionViewController.swift
//  Trevel App
//
//  Created by BOTTAK on 4/27/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit


class ConclusionViewController: UIViewController {
    
    
    
    @IBOutlet weak var consTextLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateStopViewController: ConclusionViewControllerDelegate {
    func conclusionViewController(_ stop: RealmStop) {
        stepLabel.text = String(stop.rating)
        spendMoneyLabel.text = stop.spendMoney
        // self.mapsSave.text = stop.
        nameTextField.text = stop.name
    }
    
    
}
