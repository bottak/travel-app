//
//  CustomXibCell.swift
//  Trevel App
//
//  Created by BOTTAK on 6/14/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class CustomXibCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
