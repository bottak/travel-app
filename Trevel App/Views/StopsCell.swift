//
//  StopsCell.swift
//  Trevel App
//
//  Created by BOTTAK on 5/3/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class StopsCell: UITableViewCell {
    
    
    @IBOutlet weak var stopTitleLabel: UILabel!
    
    @IBOutlet weak var stopSubtitleLabel: UILabel!
    
    @IBOutlet weak var stopSpendMoney: UILabel!
    
    @IBOutlet weak var samoletImage: UIImageView!
    

    @IBOutlet weak var poezdImage: UIImageView!
    
    
    @IBOutlet weak var autobusImage: UIImageView!
    
    
    @IBOutlet weak var tableView: UIView!
    
    
    
    
    @IBOutlet weak var star1: UIImageView!
    
    @IBOutlet weak var star2: UIImageView!
    
    @IBOutlet weak var star3: UIImageView!
    
    @IBOutlet weak var star4: UIImageView!
    
    @IBOutlet weak var star5: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
