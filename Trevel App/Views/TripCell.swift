//
//  TripCell.swift
//  Trevel App
//
//  Created by BOTTAK on 4/26/19.
//  Copyright © 2019 Stanislau Reut. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var travelNameLabel: UILabel!
    
    @IBOutlet weak var travelSubtitleLabel: UILabel!
    
    @IBOutlet weak var star1: UIImageView!
    
    @IBOutlet weak var star2: UIImageView!
    
    @IBOutlet weak var star3: UIImageView!
    
    @IBOutlet weak var star4: UIImageView!
    
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var travelView: UIView!
    
    
    
    
}
